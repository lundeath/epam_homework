package com.epam.ukrainets;


import com.epam.ukrainets.comparator.GunsComparator;
import com.epam.ukrainets.schema.JSONSchemeValidator;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class JSONUser {

    public static void main(String[] args) throws IOException, ProcessingException {
        File json = new File("src/main/resources/gunsJSON.json");
        File schema = new File("src/main/resources/gunsJSONSchema.json");

        checkValid(schema, json);
        JSONParser parser = new JSONParser();
        printList(parser.getGunsList(json));
    }

    private static void printList(List<Guns> guns) {
        System.out.println("JSON:");
        Collections.sort(guns, new GunsComparator());
        for (Guns gun : guns) {
            System.out.println(gun);
        }
    }

    private static void checkValid(File schema, File json) throws IOException, ProcessingException {
        if (JSONSchemeValidator.isJsonValid(schema, json)) {
            System.out.println("JSON Schema is valid!");
        } else {
            System.out.println("JSON Schema is NOT valid!");
        }
    }
}
