package com.epam.ukrainets;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Guns {
    @JsonProperty("gunNo")
    private int gunNo;
    @JsonProperty("model")
    private String model;
    @JsonProperty("handy")
    private boolean handy;
    @JsonProperty("origin")
    private String origin;
    @JsonProperty("material")
    private String material;
    @JsonProperty("ttc")
    private TTC ttc;

    public Guns() {
    }

    public Guns(int gunNo, String model, boolean handy, String origin, String material, TTC ttc) {
        this.gunNo = gunNo;
        this.model = model;
        this.handy = handy;
        this.origin = origin;
        this.material = material;
        this.ttc = ttc;
    }

    public String getModel() {
        return model;
    }

    public int getGunNo() {
        return gunNo;
    }

    public void setGunNo(int gunNo) {
        this.gunNo = gunNo;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean getHandy() {
        return handy;
    }

    public void setHandy(boolean handy) {
        this.handy = handy;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public TTC getTtc() {
        return ttc;
    }

    public void setTtc(TTC ttc) {
        this.ttc = ttc;
    }

    @Override
    public String toString() {
        return "Guns{" +
                "model='" + model + '\'' +
                ", handy=" + handy +
                ", origin='" + origin + '\'' +
                ", material='" + material + '\'' +
                ", ttc=" + ttc +
                '}';
    }
}
