package com.epam.ukrainets;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONParser {
    private ObjectMapper objectMapper;

    public JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Guns> getGunsList(File jsonFile){
        Guns[] guns = new Guns[0];
        try{
            guns = objectMapper.readValue(jsonFile, Guns[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(guns);
    }
}
