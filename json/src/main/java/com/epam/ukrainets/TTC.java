package com.epam.ukrainets;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TTC {
    @JsonProperty("distance")
    private String distance;
    @JsonProperty("sightability")
    private int sightAbility;
    @JsonProperty("hasclip")
    private boolean hasClip;
    @JsonProperty("hasoptics")
    private boolean hasOptics;

    public TTC() {
    }

    public TTC(String distance, int sightAbility, boolean hasClip, boolean hasOptics) {
        this.distance = distance;
        this.sightAbility = sightAbility;
        this.hasClip = hasClip;
        this.hasOptics = hasOptics;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getSightAbility() {
        return sightAbility;
    }

    public void setSightAbility(int sightAbility) {
        this.sightAbility = sightAbility;
    }

    public boolean isHasClip() {
        return hasClip;
    }

    public void setHasClip(boolean hasClip) {
        this.hasClip = hasClip;
    }

    public boolean isHasOptics() {
        return hasOptics;
    }

    public void setHasOptics(boolean hasOptics) {
        this.hasOptics = hasOptics;
    }

    @Override
    public String toString() {
        return "TTC{" +
                "distance='" + distance + '\'' +
                ", sightAbility=" + sightAbility +
                ", hasClip=" + hasClip +
                ", hasOptics=" + hasOptics +
                '}';
    }
}
