import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    private static BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        printNumbers();
    }

    public static void printNumbers() throws IOException {
        ArrayList<Integer> odd = new ArrayList<Integer>();
        ArrayList<Integer> even = new ArrayList<Integer>();
        int oddSum = 0;
        int evenSum = 0;


        System.out.println("Enter min limit of interval");
        int min = Integer.parseInt(buf.readLine());
        System.out.println("Enter max limit of interval");
        int max = Integer.parseInt(buf.readLine());

        //writing numbers to fitting collections
        for (int i = min; i <= max; i++) {
            if (i % 2 == 0) {
                odd.add(i);
            } else {
                even.add(i);
            }
        }

        System.out.println("\nOdd numbers: ");

        for (int i : odd
                ) {
            System.out.print(i + " ");
            oddSum += i;
        }
        System.out.println("\nSummary of odd numbers: " + oddSum);
        System.out.println("\nEven numbers: ");

        //making even array list in reverse order
        Collections.reverse(even);
        for (int i : even
                ) {
            System.out.print(i + " ");
            evenSum += i;
        }
        System.out.println("\nSummary of even numbers: " + evenSum);

        buildFibonacci(odd.get(odd.size() - 1), even.get(0));
    }

    public static void buildFibonacci(int maxOdd, int maxEven) throws IOException {
        System.out.println("Enter the size of set");
        int n = Integer.parseInt(buf.readLine());
        ArrayList<Integer> odd = new ArrayList<Integer>();
        ArrayList<Integer> even = new ArrayList<Integer>();

        int num = 0;
        int num2 = 1;
        int fibonacci;


        //building fibonacci series from 1
        for (int i = 0; i < n; i++) {
            fibonacci = num + num2;
            num = num2;
            num2 = fibonacci;
            if (num % 2 == 0) {
                if(num<=maxOdd)
                odd.add(num);
            } else {
                if(num<=maxEven)
                even.add(num);
            }
        }


        System.out.println("\nOdd Fibonacci series: " + odd);
        System.out.println("Odd percent: " + (odd.size()*100)/(odd.size()+even.size()) + "%");
        System.out.println("\nEven Fibonacci series: " + even);
        System.out.println("Even percent: " + (even.size()*100)/(odd.size()+even.size()) + "%");

    }
}
