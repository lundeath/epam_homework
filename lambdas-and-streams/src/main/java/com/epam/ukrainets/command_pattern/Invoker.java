package com.epam.ukrainets.command_pattern;

import com.epam.ukrainets.command_pattern.commands.Command;

public class Invoker {
    private Command command;
    public void setCommand(Command command){
        this.command = command;
    }
    public void doCommand(){
        command.execute();
    }
}
