package com.epam.ukrainets.command_pattern.commands;

import com.epam.ukrainets.command_pattern.Receiver;

public class GoCommand implements Command{
    Receiver receiver;
    String arg;
    public GoCommand(Receiver receiver, String arg) {
        this.receiver = receiver;
        this.arg = arg;
    }

    @Override
    public void execute() {
        receiver.go(arg);
    }
}
