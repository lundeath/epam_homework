package com.epam.ukrainets.command_pattern;

import com.epam.ukrainets.command_pattern.commands.*;

public class Client {
    public static void main(String[] args) {
        Invoker invoker = new Invoker();
        Receiver receiver = new Receiver();
        Command doGo = new GoCommand(receiver, "Go!");
        Command doSwim = new SwimCommand(receiver, "Swim!");
        Command doEat = new EatCommand(receiver, "Eat!");
        Command doSleep = new SleepCommand(receiver, "Sleep!");
        //invoke commands
        invoker.setCommand(doGo);
        invoker.doCommand();

        invoker.setCommand(doEat);
        invoker.doCommand();

        invoker.setCommand(doSwim);
        invoker.doCommand();

        invoker.setCommand(doSleep);
        invoker.doCommand();
    }
}
