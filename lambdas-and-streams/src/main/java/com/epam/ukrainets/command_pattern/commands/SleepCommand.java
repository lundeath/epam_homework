package com.epam.ukrainets.command_pattern.commands;

import com.epam.ukrainets.command_pattern.Printer;
import com.epam.ukrainets.command_pattern.Receiver;

public class SleepCommand implements Command, Printer {
    Receiver receiver;
    String arg;

    public SleepCommand() {
    }

    public SleepCommand(Receiver receiver, String arg) {
        this.receiver = receiver;
        this.arg = arg;
    }

    @Override
    public void execute() {
        receiver.sleep(arg);
    }


    @Override
    public void print(String arg) {

    }
}
