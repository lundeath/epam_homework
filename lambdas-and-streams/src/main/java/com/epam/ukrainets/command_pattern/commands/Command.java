package com.epam.ukrainets.command_pattern.commands;

public interface Command {
    void execute();
}
