package com.epam.ukrainets.command_pattern.commands;

import com.epam.ukrainets.command_pattern.Receiver;

public class SwimCommand implements Command{
    Receiver receiver;
    String arg;
    public SwimCommand(Receiver receiver, String arg) {
        this.receiver = receiver;
        this.arg = arg;
    }

    @Override
    public void execute() {
        receiver.swim(arg);
    }
}
