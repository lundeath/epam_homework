package com.epam.ukrainets.command_pattern.commands;

import com.epam.ukrainets.command_pattern.Printer;
import com.epam.ukrainets.command_pattern.Receiver;

public class EatCommand implements Command, Printer {
    Receiver receiver;
    String arg;

    public EatCommand() {
    }

    public EatCommand(Receiver receiver, String arg) {
        this.receiver = receiver;
        this.arg = arg;
    }

    @Override
    public void execute() {
        receiver.eat(arg);
    }

    @Override
    public void print(String arg) {
        System.out.println(arg);
    }
}
