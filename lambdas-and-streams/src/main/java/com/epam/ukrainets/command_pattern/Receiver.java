package com.epam.ukrainets.command_pattern;

import com.epam.ukrainets.command_pattern.commands.EatCommand;
import com.epam.ukrainets.command_pattern.commands.SleepCommand;

public class Receiver {
    //lambda function
    public void go(String arg) {
        Printer printer = (argument) -> System.out.println(argument);
        printer.print(arg);
    }
    //Method reference
    public void swim(String arg) {
        Printer printer = System.out::println;
        printer.print(arg);
    }
    //Anonymous class
    public void sleep(String arg) {
        SleepCommand sleep = new SleepCommand(){
            @Override
            public void print(String arg) {
                System.out.println(arg);
            }
        };
        sleep.print(arg);
    }
    //Object of command class
    public void eat(String arg) {
        new EatCommand().print(arg);
    }
}
