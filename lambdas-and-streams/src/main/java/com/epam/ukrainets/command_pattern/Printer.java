package com.epam.ukrainets.command_pattern;

public interface Printer {
    void print(String arg);
}
