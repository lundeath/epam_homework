package com.epam.ukrainets.stream_generators;

import java.util.ArrayList;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = randomList();
        arrayList.forEach((i) -> System.out.print(i + " "));
        Optional<Integer> maxValue = arrayList.stream().max(Integer::compareTo);
        Optional<Integer> minValue = arrayList.stream().min(Integer::compareTo);
        Optional<Integer> sum = arrayList.stream().reduce((left, right) -> left + right);
        OptionalDouble average = arrayList.stream().mapToDouble(a -> a).average();
        System.out.println("\nMax value: " + maxValue);
        System.out.println("Min value: " + minValue);
        System.out.println("Sum: " + sum);
        System.out.println("Average: " + average);
    }

    public static ArrayList<Integer> randomList() {
        ArrayList<Integer> list = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            list.add(random.nextInt(255));
        }
        return list;
    }

}
