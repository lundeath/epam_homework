package com.epam.ukrainets.functional_interface;

public class Main {
    public static void main(String[] args) {
        Calculate calc = (int a, int b, int c) -> {
            if (a > b & a > c)
                return a;
            else if (b > a & b > c)
                return b;
            return c;
        };
        Calculate calc2 = (int a, int b, int c) -> (a + b + c) / 3;
        System.out.println("Max : " + calc.calculate(1, 2, 3) +
                "\nAver : " + calc2.calculate(1,2,3));
    }

}
