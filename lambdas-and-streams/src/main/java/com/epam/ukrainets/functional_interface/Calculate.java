package com.epam.ukrainets.functional_interface;


public interface Calculate {
    int calculate(int a, int b, int c);
}
