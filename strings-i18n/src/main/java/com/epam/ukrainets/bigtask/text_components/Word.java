package com.epam.ukrainets.bigtask.text_components;

public class Word extends Text{
    private String word;

    public Word(String word) {
        this.word = word;
    }

    public Word() {
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
