package com.epam.ukrainets;

import java.io.*;
import java.util.ArrayList;

/**
 * Singleton class
 * Reading from file
 */
public class Reader {
    private static Reader instance;
    public static synchronized Reader getInstance() {
        if (instance == null) {
            instance = new Reader();
        }
        return instance;
    }

    private Reader() {
    }

    public ArrayList<String> readFromFile() {
        ArrayList<String> fileStrings = new ArrayList<String>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File("C:\\Users\\Admin" +
                    "\\IdeaProjects\\epam_homework\\bigtask\\src\\main\\resources\\matt_weisfeld.txt")));
            while (reader.ready()) {
                fileStrings.add(reader.readLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File is not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileStrings;
    }

}
