package com.epam.ukrainets.bigtask.text_components;

import java.util.ArrayList;

public class Sentence extends Text{
    private String sentence;

    public Sentence(String sentence) {
        this.sentence = sentence;
    }

    public Sentence() {
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

}
