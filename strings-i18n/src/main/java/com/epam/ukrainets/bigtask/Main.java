package com.epam.ukrainets.bigtask;

import com.epam.ukrainets.Reader;
import com.epam.ukrainets.bigtask.text_components.Text;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> fileStrings = Reader.getInstance().readFromFile();
        Text text = new Text(fileStrings);
        text.splitTextToComponents();
        //text.getSentences().forEach(item -> System.out.println(item.getSentence()));
        //text.getWords().forEach(item -> System.out.println(item.getWord()));
    }
}
