package com.epam.ukrainets.bigtask.text_components;

import java.util.ArrayList;

public class Text {
    private ArrayList<String> text;
    private ArrayList<Sentence> sentences = new ArrayList<>();
    private ArrayList<Word> words = new ArrayList<>();
    private ArrayList<Punctuation> punctuations = new ArrayList<>();

    public Text() {
    }

    public Text(ArrayList<String> text) {
        this.text = text;
    }

    public ArrayList<String> getText() {
        return text;
    }

    public ArrayList<Sentence> getSentences() {
        return sentences;
    }

    public ArrayList<Word> getWords() {
        return words;
    }

    public ArrayList<Punctuation> getPunctuations() {
        return punctuations;
    }

    public void splitTextToComponents() {
        String textString = textArrToString();
        String[] sentencesArr;
        String[] wordsArr;
        sentencesArr = textString.split("(?<=[.!?])\\s+(?=[A-Z])");
        for (int i = 0; i < sentencesArr.length; i++) {
            sentences.add(new Sentence(sentencesArr[i]));
        }
        for (Sentence sentence : sentences
                ) {
            wordsArr = sentence.getSentence().split("\\s|\\.|\\W[?=\\s]");
            for (int i = 0; i < wordsArr.length; i++) {
                words.add(new Word(wordsArr[i]));
            }
        }
    }

    private String textArrToString() {
        String textString = "";
        for (String item : text
                ) {
            textString += item;
        }
        return textString;
    }
}
