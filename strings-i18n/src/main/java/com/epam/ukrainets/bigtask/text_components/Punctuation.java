package com.epam.ukrainets.bigtask.text_components;

public class Punctuation extends Text{
    private String punctiation;

    public Punctuation(String punctiation) {
        this.punctiation = punctiation;
    }

    public Punctuation() {
    }

    public String getPunctiation() {
        return punctiation;
    }

    public void setPunctiation(String punctiation) {
        this.punctiation = punctiation;
    }
}
