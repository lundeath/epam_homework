package com.epam.ukrainets.View;

import com.epam.ukrainets.tasks.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    Locale locale;
    ResourceBundle bundle;

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::internationalizeMenuUkrainian);
        methodsMenu.put("3", this::internationalizeMenuEnglish);
        methodsMenu.put("4", this::testRegEx);
        methodsMenu.put("5", this::split);
    }

    private void testStringUtils() {
        StringUtils utils = new StringUtils();
        utils.addToParameters(1)
                .addToParameters(0.5)
                .addToParameters("Some String")
                .addToParameters('s');
        System.out.println(utils.concat());
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void testRegEx() {
        String message = "This is a test message that starts with capital letter" +
                " and ends with period.";
        System.out.println(message);
        System.out.println("Regex: ^[A-Z].*.$");
        Pattern pattern = Pattern.compile("(^[A-Z].*\\.$)");
        Matcher matcher = pattern.matcher(message);
        System.out.println(matcher.matches());
        System.out.println();
        String replace = "This message is for testing all vowels replacement";
        System.out.println(replace);
        System.out.println("Regex: [aeiou]");
        System.out.println(vowels(replace));

    }

    private String vowels(String message){
        String replaced = message.replaceAll("[aeiou]","_");
        return replaced;
    }

    private void split() {
        String string = "I want to show the splitting method. You can see that it works :)";
        System.out.println("String:");
        System.out.println(string);
        String[] splitted = string.split("(the)|(You)");
        System.out.println("Modificated string:");
        Arrays.stream(splitted).forEach(System.out::print);
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
