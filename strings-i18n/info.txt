1.	Read recommended sources.
2.	Create class StringUtils with an undefined number of parameters of any class
    that concatenates all parameters and returns Strings.
3.	Internationalize menu in “Battle of Droids”. (native2ascii)
    https://native2ascii.net/
4.	Using the documentation for java.util.regex.Pattern as a resource,
    write and test a regular expression that checks a sentence to see
    that it begins with a capital letter and ends with a period.
5.	Split some string on the words "t he" or "you".
6.	Replace all the vowels in some text with underscores.
7.	Solve the RexEx Crossword: http://uzer.com.ua/cross/


