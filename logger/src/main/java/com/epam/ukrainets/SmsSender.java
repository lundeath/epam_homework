package com.epam.ukrainets;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsSender {
    // Find your Account Sid and Auth Token at twilio.com/console
    public static final String ACCOUNT_SID =
                    "ACff257385f05ab25f72cf9e258066b6e2";
    public static final String AUTH_TOKEN =
            "24b5635d6df221024c5e9b4610d2d163";

    public static void sendSMS(String body) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message
                .creator(new PhoneNumber("+380630686604"), // to
                        new PhoneNumber("+13852573806"), // from
                        body)
                .create();

        System.out.println("SID of SMS: " + message.getSid());
    }
}
