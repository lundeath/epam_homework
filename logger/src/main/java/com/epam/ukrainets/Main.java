package com.epam.ukrainets;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        GmailAppender appender = new GmailAppender();
        appender.createSession();
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        //logger.error("This is a error message"); <! no SSL to gmail>
        SmsSender.sendSMS("This is fatal message");
    }
}
