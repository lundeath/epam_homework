package com.epam.ukrainets.blocking_queue;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockQueue {

    public static void main(String[] args) throws IOException {

        final BlockingQueue<String> queue =  new LinkedBlockingQueue();

        Thread thread1 = new Thread(() -> {
            try {
                queue.put("Hello world, queue!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


        Thread thread2 = new Thread(() -> {
            try {
                System.out.println(queue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        thread1.start();
        thread2.start();

    }
}
