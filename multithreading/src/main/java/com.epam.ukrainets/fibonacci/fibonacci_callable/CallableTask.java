package com.epam.ukrainets.fibonacci.fibonacci_callable;

import com.epam.ukrainets.fibonacci.fibonacci_thread.Task;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.StampedLock;

/**
 * This Class is using locking
 */

public class CallableTask extends Task implements Callable {
    private int n;

    public CallableTask(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public Integer call() throws Exception {
        int sum = 0;
        synchronized (this) {
            for (int i = 0; i < n; i++) {
                sum += fibonacci(i);
            }
            return sum;
        }
    }
}
