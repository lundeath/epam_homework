package com.epam.ukrainets.fibonacci;

import com.epam.ukrainets.fibonacci.fibonacci_callable.CallableTask;
import com.epam.ukrainets.fibonacci.fibonacci_executors.ExecutorTask;
import com.epam.ukrainets.fibonacci.fibonacci_executors.ScheduledTask;
import com.epam.ukrainets.fibonacci.fibonacci_thread.Task;

public class FibonacciMain {
    public static void main(String[] args) throws Exception {
        //First task
        Task task = new Task(15);
        task.run();

        //Second task
        ExecutorTask executorTask = new ExecutorTask(15);
        executorTask.buildFibonacci();
        ScheduledTask scheduledTask = new ScheduledTask(15);
        scheduledTask.buildFibonacci();

        //Third task
        CallableTask callableTask = new CallableTask(15);
        System.out.println("\nSum of fibonacci(" + callableTask.getN() + ") via Callable: "
                + callableTask.call());
    }
}
