package com.epam.ukrainets.fibonacci.fibonacci_thread;


public class Task implements Runnable{
    private int n;
    public Task(int n) {
        this.n = n;
    }

    public Task() {
    }

    public void run() {
        System.out.println("Fibonacci sequence: ");
        for (int i = 0; i < n; i++) {
            System.out.print(fibonacci(i)+ " ");
        }
        System.out.println();
    }

    public long fibonacci(int n){
        if (n <= 1) return n;
        else return fibonacci(n-1) + fibonacci(n-2);
    }
}
