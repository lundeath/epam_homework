package com.epam.ukrainets.fibonacci.fibonacci_executors;

import com.epam.ukrainets.fibonacci.fibonacci_thread.Task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorTask extends Task {
    private ExecutorService service = Executors.newCachedThreadPool();
    private int n;

    public ExecutorTask(int n) {
        this.n = n;
    }

    public void buildFibonacci() {
        service.submit(new Runnable() {
            public void run() {
                System.out.println("Fibonacci sequence (via ExecutorTask): ");
                for (int i = 0; i < n; i++) {
                    System.out.print(fibonacci(i) + " ");
                }
                service.shutdown();
            }
        });
    }

}
