package com.epam.ukrainets.fibonacci.fibonacci_executors;

import com.epam.ukrainets.fibonacci.fibonacci_thread.Task;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledTask extends Task {
    ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
    private int n;

    public ScheduledTask(int n) {
        this.n = n;
    }

    public void buildFibonacci(){
        service.scheduleAtFixedRate(new Runnable() {
            public void run() {
                System.out.println("Fibonacci sequence (via ScheduledTask): ");
                for (int i = 0; i < n; i++) {
                    System.out.print(fibonacci(i) + " ");
                }
                service.shutdown();
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

}
