package com.epam.ukrainets.ping_pong;

public class PingPongThread extends Thread {
    private String msg;
    private static String turn;

    public PingPongThread(String msg){
        this.msg = msg;
    }
    @Override
    public void run() {
        while(true) {
            try {
                playTurn();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    public void playTurn() throws InterruptedException {
            synchronized(PingPongThread.class) {
                if (!msg.equals(turn)){
                    turn=msg;
                    Thread.sleep(1000);
                    System.out.println(msg);
                }
        }
    }
}