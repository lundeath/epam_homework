package com.epam.ukrainets.ping_pong;

public class ThreadTest {
    public static void main(String[] args) {
        PingPongThread thread1 = new PingPongThread("Ping");
        PingPongThread thread2 = new PingPongThread("Pong");
        thread1.start();
        thread2.start();
    }
}
