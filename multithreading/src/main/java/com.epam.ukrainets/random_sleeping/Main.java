package com.epam.ukrainets.random_sleeping;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter quantity of scheduled tasks: ");
        ThreadPool pool = new ThreadPool(Integer.parseInt(br.readLine()));
        pool.createPool();


    }
}
