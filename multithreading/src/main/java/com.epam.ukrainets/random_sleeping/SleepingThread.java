package com.epam.ukrainets.random_sleeping;

public class SleepingThread implements Runnable{
    public void run() {
        int randomValue = 1 + (int) (Math.random() * 10);
        try {
            Thread.sleep(randomValue);
            System.out.println(Thread.currentThread().getName()+" slept " + randomValue + " seconds.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
