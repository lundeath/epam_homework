package com.epam.ukrainets.random_sleeping;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadPool {
    private int quantity;

    public ThreadPool(int quantity) {
        this.quantity = quantity;
    }

    public void createPool() {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(10);

        for (int i = 0; i < quantity; i++) {
            pool.execute(new SleepingThread());
        }
        pool.shutdown();
    }
}
