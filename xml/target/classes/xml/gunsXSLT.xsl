<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 1px ;
                    }

                    td.colfmt {
                    border: 1px ;
                    background-color: white;
                    color: black;
                    text-align:right;
                    }

                    th {
                    background-color: #2E9AFE;
                    color: white;
                    }

                </style>
            </head>

            <body>
                <table class="tfmt">
                    <tr>
                        <th style="width:250px">gunNo</th>
                        <th style="width:250px">Model</th>
                        <th style="width:250px">Handy</th>
                        <th style="width:250px">Origin</th>
                        <th style="width:250px">Material</th>
                        <th style="width:250px">Distance</th>
                        <th style="width:250px">Sight ability</th>
                        <th style="width:250px">Has clip</th>
                        <th style="width:250px">Has optics</th>
                        
                    </tr>

                    <xsl:for-each select="guns/gun">

                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="@gunNo" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="model" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="handy" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="origin" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="material" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="distance" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="sightability" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="hasclip" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="hasoptics" />
                            </td>
                        </tr>

                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>