package com.epam.ukrainets.comparator;

import com.epam.ukrainets.model.Guns;

import java.util.Comparator;

public class GunsComparator implements Comparator<Guns> {
    @Override
    public int compare(Guns o1, Guns o2) {
        return Integer.compare(o1.getTtc().getSightAbility(), o2.getTtc().getSightAbility());
    }
}
