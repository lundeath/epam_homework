package com.epam.ukrainets.validator;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MyValidator {
    public void validate(File schemaFile, File xmlFile) throws MalformedURLException {
        Source _xmlFile = new StreamSource(xmlFile);
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(_xmlFile);
            System.out.println(_xmlFile.getSystemId() + " is valid");
        } catch (SAXException e) {
            System.out.println(_xmlFile.getSystemId() + " is NOT valid reason:" + e);
        } catch (IOException e) {}
    }

}
