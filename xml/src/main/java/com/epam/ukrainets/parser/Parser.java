package com.epam.ukrainets.parser;



import com.epam.ukrainets.comparator.GunsComparator;
import com.epam.ukrainets.convertor.XmlToHtml;
import com.epam.ukrainets.filechecker.ExtensionChecker;
import com.epam.ukrainets.model.Guns;
import com.epam.ukrainets.parser.dom.DOMParserUser;
import com.epam.ukrainets.parser.sax.SAXHandler;
import com.epam.ukrainets.parser.sax.SAXParserUser;
import com.epam.ukrainets.parser.stax.StAXReader;
import com.epam.ukrainets.validator.MyValidator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Parser {

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        File xml = new File("src\\main\\resources\\xml\\gunsXML.xml");
        File xsd = new File("src\\main\\resources\\xml\\gunsXSD.xsd");
        File xslt = new File("src\\main\\resources\\xml\\gunsXSLT.xsl");



        MyValidator validator = new MyValidator(); //validation
        validator.validate(xsd, xml);

        if (checkIfXML(xml) && checkIfXSD(xsd)) { //parsing
            printList(DOMParserUser.getGunsList(xml, xsd), "DOM");
            printList(SAXParserUser.parseGuns(xml, xsd), "SAX");
            printList(StAXReader.parseGuns(xml, xsd), "StAX");
        }

        XmlToHtml.convertXMLToHTML(xml, xslt);

    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void printList(List<Guns> guns, String parserName) {
        Collections.sort(guns, new GunsComparator());
        System.out.println(parserName);
        for (Guns gun : guns) {
            System.out.println(gun);
        }
    }

}
