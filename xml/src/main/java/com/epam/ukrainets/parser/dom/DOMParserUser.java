package com.epam.ukrainets.parser.dom;

import com.epam.ukrainets.model.Guns;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DOMParserUser {
    public static List<Guns> getGunsList(File xml, File xsd){
        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();

        DOMDocReader reader = new DOMDocReader();

        return reader.readDoc(doc);
    }
}
