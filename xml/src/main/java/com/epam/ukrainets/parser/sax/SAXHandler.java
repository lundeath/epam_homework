package com.epam.ukrainets.parser.sax;

import com.epam.ukrainets.model.Guns;
import com.epam.ukrainets.model.TTC;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.*;

import java.util.ArrayList;

public class SAXHandler extends DefaultHandler {
    private ArrayList<Guns> guns = new ArrayList<>();
    private Guns gun = null;
    private TTC ttc = null;

    private boolean bModel = false;
    private boolean bHandy = false;
    private boolean bOrigin = false;
    private boolean bMaterial = false;
    private boolean bTTC = false;
    private boolean bDistance = false;
    private boolean bSight = false;
    private boolean bHasClip = false;
    private boolean bHasOptics = false;

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("gun")){
            String gunN = attributes.getValue("gunNo");
            gun = new Guns();
            gun.setGunNo(Integer.parseInt(gunN));
        }
        else if (qName.equalsIgnoreCase("model")){bModel = true;}
        else if (qName.equalsIgnoreCase("handy")){bHandy = true;}
        else if (qName.equalsIgnoreCase("origin")){bOrigin = true;}
        else if (qName.equalsIgnoreCase("material")){bMaterial = true;}
        else if (qName.equalsIgnoreCase("ttc")){bTTC = true;}
        else if (qName.equalsIgnoreCase("distance")){bDistance = true;}
        else if (qName.equalsIgnoreCase("sightability")){bSight = true;}
        else if (qName.equalsIgnoreCase("hasclip")){bHasClip = true;}
        else if (qName.equalsIgnoreCase("hasoptics")){bHasOptics = true;}
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("gun")){
            guns.add(gun);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (bModel) {
            gun.setModel(new String(ch, start, length));
            bModel = false;
        }
        if (bHandy) {
            gun.setHandy(Boolean.valueOf(new String(ch, start, length)));
            bHandy = false;
        }
        if (bOrigin) {
            gun.setOrigin(new String(ch, start, length));
            bOrigin = false;
        }
        if (bMaterial) {
            gun.setMaterial(new String(ch, start, length));
            bMaterial = false;
        }
        if (bTTC) {
            ttc = new TTC();
            bTTC = false;
        }
        if (bDistance) {
            ttc.setDistance(new String(ch, start, length));
            bDistance = false;
        }
        if (bSight) {
            ttc.setSightAbility(new Integer(new String(ch, start, length)));
            bSight = false;
        }
        if (bHasClip) {
            ttc.setHasClip(Boolean.valueOf(new String(ch, start, length)));
            bHasClip = false;
        }
        if (bHasOptics) {
            ttc.setHasOptics(Boolean.valueOf(new String(ch, start, length)));
            gun.setTtc(ttc);
            bHasOptics = false;
        }
    }

    @Override
    public void endDocument() {}

    public ArrayList<Guns> getGuns() {
        return guns;
    }
}
