package com.epam.ukrainets.parser.stax;

import com.epam.ukrainets.model.Guns;
import com.epam.ukrainets.model.TTC;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {
    public static List<Guns> parseGuns(File xml, File xsd){
        List<Guns> gunList = new ArrayList<>();
        Guns gun = null;
        TTC ttc = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "gun":
                            gun = new Guns();

                            Attribute idAttr = startElement.getAttributeByName(new QName("gunNo"));
                            if (idAttr != null) {
                                gun.setGunNo(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "model":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setModel(xmlEvent.asCharacters().getData());
                            break;
                        case "handy":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setHandy(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "material":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setMaterial(xmlEvent.asCharacters().getData());
                            break;
                        case "ttc":
                            xmlEvent = xmlEventReader.nextEvent();
                            ttc = new TTC();
                            break;
                        case "distance":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ttc != null;
                            ttc.setDistance(xmlEvent.asCharacters().getData());
                            break;
                        case "sightability":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ttc != null;
                            ttc.setSightAbility(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "hasclip":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ttc != null;
                            ttc.setHasClip(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "hasoptics":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ttc != null;
                            ttc.setHasOptics(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            gun.setTtc(ttc);
                            break;
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("gun")){
                        gunList.add(gun);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return gunList;
    }
}
