package com.epam.ukrainets.parser.dom;

import com.epam.ukrainets.model.Guns;
import com.epam.ukrainets.model.TTC;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Guns> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Guns> guns = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("gun");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Guns gun = new Guns();
            TTC ttc;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;

                gun.setGunNo(Integer.parseInt(element.getAttribute("gunNo")));
                gun.setModel(element.getElementsByTagName("model").item(0).getTextContent());
                gun.setHandy(Boolean.parseBoolean(element.getElementsByTagName("handy").item(0).getTextContent()));
                gun.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
                gun.setMaterial(element.getElementsByTagName("material").item(0).getTextContent());

                ttc = getTtc(element.getElementsByTagName("ttc"));
                gun.setTtc(ttc);
                guns.add(gun);
            }
        }
        return guns;
    }

    private TTC getTtc(NodeList nodes){
        TTC ttc = new TTC();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            ttc.setDistance(element.getElementsByTagName("distance").item(0).getTextContent());
            ttc.setHasClip(Boolean.parseBoolean(element.getElementsByTagName("hasclip").item(0).getTextContent()));
            ttc.setHasOptics(Boolean.parseBoolean(element.getElementsByTagName("hasoptics").item(0).getTextContent()));
            ttc.setSightAbility(Integer.parseInt(element.getElementsByTagName("sightability").item(0).getTextContent()));
        }

        return ttc;
    }

}
