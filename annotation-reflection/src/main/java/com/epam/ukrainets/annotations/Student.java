package com.epam.ukrainets.annotations;

public class Student {
    @JsonField("json-firstName")
    String firstName;

    @JsonField("json-secondName")
    String secondName;

    public Student(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }
}
