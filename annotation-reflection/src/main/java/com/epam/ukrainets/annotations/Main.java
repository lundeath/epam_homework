package com.epam.ukrainets.annotations;

public class Main {
    public static void main(String[] args) throws JsonSerializeException {
        Student student = new Student("James", "Watson");
        JsonSerializer serializer = new JsonSerializer();
        serializer.serialize(student);
    }

}
