package com.epam.ukrainets.reflection;

import java.lang.reflect.Field;
import java.util.HashMap;

public class ReflectionAPI extends HashMap<String, String> {
    private Object parent;

    public ReflectionAPI(Object parent) {
        this.parent = parent;
    }

    public void showFields() throws Exception {
        Class<?> parentClass = parent.getClass();
        System.out.println("Package: " + parentClass.getPackage().getName());
        System.out.println("Class: " + parentClass.getName());
        Field[] fields = parentClass.getDeclaredFields();
        System.out.print("Fields: ");
        for (Field field: fields
             ) {
            field.setAccessible(true);
            System.out.print(field.getName() + "(" + field.getType() + "); ");
        }

    }

}