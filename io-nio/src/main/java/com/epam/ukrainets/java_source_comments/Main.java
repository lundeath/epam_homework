package com.epam.ukrainets.java_source_comments;

import com.epam.ukrainets.my_cmd_java.CommandLine;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        CommandLine cmd = new CommandLine();
        File[] files = cmd.getResourceFiles("C:\\Program Files\\Java\\jre1.8.0_171\\lib\\rt");
        cmd.showFiles(files);
        System.out.println("Choose class file: ");
        String className = cmd.extendPath(br.readLine());
        cmd.decompileClass(className);

    }
}
