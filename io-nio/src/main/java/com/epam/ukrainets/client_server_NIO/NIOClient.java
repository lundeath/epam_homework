package com.epam.ukrainets.client_server_NIO;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;


public class NIOClient {

    public static void main(String[] args) throws IOException, InterruptedException {

        InetSocketAddress address = new InetSocketAddress("localhost", 1111);
        SocketChannel client = SocketChannel.open(address);

        log("Connecting to Server on port 1111...");

        ArrayList<String> words = new ArrayList<String>();

        words.add("Facebook");
        words.add("Twitter");
        words.add("IBM");
        words.add("Google");
        words.add("Exit");

        for (String word : words) {

            byte[] message = word.getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(message);
            client.write(buffer);

            log("sending: " + word);
            buffer.clear();

            // wait for 2 seconds before sending next message
            Thread.sleep(2000);
        }
        client.close();
    }

    private static void log(String str) {
        System.out.println(str);
    }
}
