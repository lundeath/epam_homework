package com.epam.ukrainets.compare_performance;

import java.io.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

public class Main {
    public static void main(String[] args) throws IOException {
        buffered();
        input();
    }

    private static void buffered() throws IOException {
        long startTime = System.currentTimeMillis();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(Objects.requireNonNull(classloader.getResource("file.txt")).getPath());
        long fileSize = file.length() / (1024);
        System.out.println("File size: " + fileSize + " KB");
        BufferedReader br = new BufferedReader(new InputStreamReader(classloader.getResourceAsStream("file.txt")));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
        }
        long finishTime = System.currentTimeMillis();
        long sumMillis = finishTime - startTime;
        System.out.println("Buffered reader did job in: " + sumMillis + " millis");
    }

    private static void input() {
        long startTime = System.currentTimeMillis();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(classloader.getResource("file.txt").getPath());

        long length = file.length();
        byte[] bytes = new byte[(int) length];

        try (FileInputStream fis = new FileInputStream(file)) {
            int i = 0;
            while (fis.available() > 0) {
                bytes[i] = (byte) fis.read();
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        long finishTime = System.currentTimeMillis();
        long sumMillis = finishTime - startTime;
        System.out.println("FileInputStream did job in: " + sumMillis + " millis");
    }
}
