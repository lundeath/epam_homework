package com.epam.ukrainets.input_stream;

import java.io.*;
import java.util.Objects;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(Objects.requireNonNull(classloader.getResource("file.txt")).getPath());
        long length = file.length();
        byte[] bytes = new byte[(int) length];
        InputStream inputStream = new FileInputStream(file);

        try (MyInputStream fis = new MyInputStream(inputStream, (int) length)) {
            fis.pushBack(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
