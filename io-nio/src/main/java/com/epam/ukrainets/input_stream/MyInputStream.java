package com.epam.ukrainets.input_stream;

import java.io.*;

public class MyInputStream extends PushbackInputStream {

    public MyInputStream(InputStream in, int size) {
        super(in);
        if (size <= 0) {
            throw new IllegalArgumentException("size <= 0");
        }
        this.buf = new byte[size];
        this.pos = size;
    }

    public void pushBack(byte[] arr) throws IOException {
        this.unread(arr);
    }
}
