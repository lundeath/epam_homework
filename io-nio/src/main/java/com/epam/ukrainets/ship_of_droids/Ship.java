package com.epam.ukrainets.ship_of_droids;

import java.io.Serializable;
import java.util.ArrayList;

class Ship implements Serializable {
    private ArrayList<Droid> droids = new ArrayList<Droid>();
    private transient int squareOfShip = 500;
    private int size;

    Ship(int size) {
        this.size = size;
    }

    int getAmount() {
        return droids.size();
    }


    int getSize() {
        return size;
    }


    void addDroid(Droid droid){
        droids.add(droid);
    }

}
