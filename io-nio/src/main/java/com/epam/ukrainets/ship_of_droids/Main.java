package com.epam.ukrainets.ship_of_droids;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("temp.out");
        FileInputStream fis = new FileInputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Ship ship = new Ship(15);

        for (int i = 0; i < 3; i++) {
            ship.addDroid(new Droid());
        }
        oos.writeObject(ship);
        oos.flush();
        oos.close();
        Ship readShip = (Ship) ois.readObject();
        System.out.println("Size of ship : "+readShip.getSize());
        System.out.println("Amount of droids : "+readShip.getAmount());
    }
}
