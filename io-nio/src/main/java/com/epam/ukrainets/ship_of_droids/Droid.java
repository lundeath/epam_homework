package com.epam.ukrainets.ship_of_droids;

import java.io.Serializable;

public class Droid implements Serializable {
    private final int DAMAGE = 200;
    private final int HEALTH = 1500;

    public Droid() {
    }

    public int getDamage() {
        return DAMAGE;
    }

    public int getHealth() {
        return HEALTH;
    }

}
