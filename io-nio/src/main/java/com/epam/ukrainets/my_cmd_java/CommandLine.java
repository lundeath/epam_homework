package com.epam.ukrainets.my_cmd_java;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandLine extends Thread {

    public File[] getResourceFiles(String path) throws IOException {
        File dir = new File(path);
        return dir.listFiles();
    }

    public void showFiles(File[] files) {
        for (File file : files
                ) {
            System.out.println(file.getPath());
        }
    }

    public String extendPath(String fileName) {
        return "\"C:\\Program Files\\Java\\jre1.8.0_171\\lib\\rt\\"
                + fileName + ".class\"";
    }

    public void decompileClass(String path) throws IOException {
        Runtime rt = Runtime.getRuntime();
        String command = "javap " + path;
        System.out.println("Decompiling class file...");
        System.out.println("Source code of class: " + path);
        Process proc = rt.exec(command);
        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));
        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));
        String out;
        while ((out = stdInput.readLine()) != null) {
            System.out.println(out);
        }
        while ((out = stdError.readLine()) != null) {
            System.out.println(out);
        }
    }

    public void execute() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("CMD Started... \nAvailable commands: \'cd\', \'dir\'\nExit - \"Q\"");
        String path = "C:\\";
        String command;
        try {
            while (true) {
                command = br.readLine();
                if (command.equals("Q")) {
                    System.out.println("Exiting..");
                    break;
                }
                if(command.startsWith("cd")){
                    path = command.substring(3,command.length());
                    System.out.println(path);
                } else if(command.startsWith("dir")){
                    showFiles(getResourceFiles(path));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
