package devices;

import abstract_device.Device;

public class PhoneCharger extends Device {
    public PhoneCharger(int power, boolean connection) {
        super(power, connection);
    }

    @Override
    public String toString() {
        return "Phone Charger: Power = " + this.getPower() + "Watt; Connected = " + this.isConnected() + "}\n";
    }
}
