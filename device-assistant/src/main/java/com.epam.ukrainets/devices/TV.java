package devices;

import abstract_device.Device;

public class TV extends Device {
    public TV(int power, boolean connection) {
        super(power, connection);
    }

    @Override
    public String toString() {
        return "TV : Power = " + this.getPower() + "Watt; Connected = " + this.isConnected() + "}\n";
    }
}
