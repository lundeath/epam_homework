package devices;

import abstract_device.Device;

public class Cooker extends Device {
    public Cooker(int power, boolean connection) {
        super(power, connection);
    }

    @Override
    public String toString() {
        return "Cooker: Power = " + this.getPower() + "Watt; Connected = " + this.isConnected() + "}\n";
    }
}
