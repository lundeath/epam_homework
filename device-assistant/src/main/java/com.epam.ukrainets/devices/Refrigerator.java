package devices;

import abstract_device.Device;

public class Refrigerator extends Device {
    public Refrigerator(int power, boolean connection) {
        super(power, connection);
    }

    @Override
    public String toString() {
        return "Refrigerator: Power = " + this.getPower() + "Watt; Connected = " + this.isConnected() + "}\n";
    }
}
