package devices;

import abstract_device.Device;

public class Microwave extends Device {
    public Microwave(int power, boolean connection) {
        super(power, connection);
    }

    @Override
    public String toString() {
        return "Microwave: Power = " + this.getPower() + "Watt; Connected = " + this.isConnected() + "}\n";
    }
}
