package devices;

import abstract_device.Device;

public class Computer extends Device {
    public Computer(int power, boolean connection) {
        super(power, connection);
    }

    @Override
    public String toString() {
        return "Computer: Power = " + this.getPower() + "Watt; Connected = " + this.isConnected() + "}\n";
    }
}
