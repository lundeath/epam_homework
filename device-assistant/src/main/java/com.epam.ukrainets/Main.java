import device_assistant.DeviceAssistant;
import java.io.IOException;

/**
 * Created by Yurii Ukrainets
 */

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        DeviceAssistant deviceAssistant = DeviceAssistant.getInstance();
        deviceAssistant.start();

    }
}
