package device_storage;

import abstract_device.Device;
import abstract_device.DeviceList;
import java.util.ArrayList;
import java.util.List;

public class DeviceStorage implements DeviceList {
    private List<Device> deviceList = new ArrayList<Device>();

    public DeviceStorage() {
    }

    public List<Device> getDeviceList() {
        return deviceList;
    }


    public void addDevice(Device device) {
        this.deviceList.add(device);
    }

    public void removeDevice(Device device) {
        this.deviceList.remove(device);
    }

    //bubble sort devices by power
    public void sort() {
        boolean isSorted = false;
        Device buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < this.deviceList.size() - 1; i++) {
                if (this.deviceList.get(i).getPower() < this.deviceList.get(i + 1).getPower()) {
                    isSorted = false;
                    buf = this.deviceList.get(i);
                    this.deviceList.set(i, this.deviceList.get(i + 1));
                    this.deviceList.set(i + 1, buf);
                }
            }
        }
    }

    //finding device by power
    public void find(int power1, int power2) {
        for (Device device : this.deviceList
                ) {
            if (device.getPower() >= power1) {
                if (device.getPower() <= power2) {
                    System.out.println(device);
                }
            }
        }
    }

    public void printDevices() {
        if (this.deviceList.size() < 1) {
            System.out.println("You have no devices yet");
        } else {
            for (Device device : this.deviceList
                    ) {
                System.out.print(device);
            }
        }
    }

    public void sum() {
        int sum = 0;
        for (Device device : this.deviceList
                ) {
            if (device.isConnected())
                sum += device.getPower();
        }
        System.out.println("Summary of consumed power = " + sum + " Watt");
    }

}
