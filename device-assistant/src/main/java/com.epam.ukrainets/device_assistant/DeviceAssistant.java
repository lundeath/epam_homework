package device_assistant;

import device_storage.DeviceStorage;
import devices.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class is a Singleton
 */

public class DeviceAssistant {
    private static DeviceAssistant ourInstance = new DeviceAssistant();

    public static DeviceAssistant getInstance() {
        return ourInstance;
    }

    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    private DeviceAssistant() {
    }



    public void start() throws IOException, InterruptedException {
        DeviceStorage deviceStorage = new DeviceStorage();

        deviceStorage.addDevice(new Computer(300, true));
        deviceStorage.addDevice(new PhoneCharger(250, false));
        deviceStorage.addDevice(new Cooker(1000, true));
        deviceStorage.addDevice(new Microwave(800, false));
        deviceStorage.addDevice(new Refrigerator(1000, true));
        deviceStorage.addDevice(new TV(500, true));

        System.out.println("\nWhat would you like to do?");
        System.out.println("1. Show devices\n2. Calculate sum of consumed power\n3. Sort devices\n" +
                "4. Find device by power\n");

        switch (Integer.parseInt(br.readLine())) {
            case 1:
                clearConsole();
                deviceStorage.printDevices();
                Thread.sleep(4000);
                start();
            case 2:
                clearConsole();
                deviceStorage.sum();
                Thread.sleep(4000);
                start();
            case 3:
                clearConsole();
                deviceStorage.sort();
                deviceStorage.printDevices();
                Thread.sleep(4000);
                start();
            case 4:
                System.out.println("Enter exact range of power: \nFirst border: ");
                int power1 = Integer.parseInt(br.readLine());
                System.out.println("Second border: ");
                int power2 = Integer.parseInt(br.readLine());
                deviceStorage.find(power1, power2);
                Thread.sleep(4000);
                start();
            default:
                clearConsole();
                System.out.println("Please, enter correct number!");
                start();
        }

    }

    private void clearConsole() {
        for (int i = 0; i < 100; i++) {
            System.out.println();
        }
    }


}
