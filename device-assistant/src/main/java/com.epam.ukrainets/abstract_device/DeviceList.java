package abstract_device;

public interface DeviceList {
    void sort();

    void addDevice(Device device);

    void removeDevice(Device device);

    void printDevices();

    void find(int power1, int power2);

    void sum();
}
