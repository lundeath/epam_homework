package abstract_device;

public abstract class Device {
    private int power;
    private boolean connection;

    public Device(int power, boolean connection) {
        this.power = power;
        this.connection = connection;
    }

    public int getPower() {
        return power;
    }

    public boolean isConnected() {
        return connection;
    }

    @Override
    public String toString() {
        return "Device{" +
                "power=" + power +
                ", connection=" + connection +
                '}';
    }

}
