package com.epam.ukrainets;

import com.epam.ukrainets.my_arraylist.MyArrayList;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        MyArrayList arrayList = new MyArrayList();
        arrayList.add("One");
        arrayList.add("Two");
        arrayList.add("Three");
        arrayList.print();
    }

}
