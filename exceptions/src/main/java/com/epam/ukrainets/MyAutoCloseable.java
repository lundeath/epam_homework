package com.epam.ukrainets;


public class MyAutoCloseable implements AutoCloseable{

    public void say() throws Exception{
        System.out.println("I'm working!");
    }

    public void close() throws Exception {
        System.out.println("MyAutoCloseable is closed!");
        throw new Exception();
    }
}
