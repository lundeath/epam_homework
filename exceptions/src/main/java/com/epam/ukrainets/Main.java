package com.epam.ukrainets;

public class Main {
    public static void main(String[] args) throws Exception {
        //try-with-resources
        try (MyAutoCloseable myAutoCloseable = new MyAutoCloseable()) {
            myAutoCloseable.say();
        }
    }
}
