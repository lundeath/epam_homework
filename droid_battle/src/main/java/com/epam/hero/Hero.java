package com.epam.hero;

import com.epam.droids.Droid;
import com.epam.droids.Ranger;
import com.epam.droids.Specialist;
import com.epam.droids.Warrior;
import java.util.Random;

public class Hero {

  private Droid droid;

  public Hero(Droid droid) {
    this.droid = droid;
    droid.setHeroName("Bot");
  }

  public Hero() {
    createRandomDroid();
    droid.setHeroName("Bot");
  }

  public Droid getDroid() {
    return droid;
  }

  private void createRandomDroid() {
    Random rand = new Random();
    int random = rand.nextInt(3);
    switch (random) {
      case 0:
        this.droid = new Ranger();
        break;
      case 1:
        this.droid = new Warrior();
        break;
      case 2:
        this.droid = new Specialist();
        break;
      default:
        throw new RuntimeException("Unexpected random value");
    }
  }
}
