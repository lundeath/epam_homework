package com.epam.droids;

import com.epam.battle.GamePanel;
import java.util.concurrent.ThreadLocalRandom;

public class Specialist extends Droid {

  private static final int HEALTH = 1000;
  private static final int MINIMUM_DAMAGE = 50;
  private static final int MAXIMUM_DAMAGE = 100;
  private static final float PROBABILITY_OF_DOUBLE_SHOOT = 0.08f;
  private static final int SPECIAL_ABILITY_CHARGE = 3;
  private static final int COUNT_SHOOT_AND_MISS = 3;


  private static final int MINI_DROID_MINIMUM_DAMAGE = 50;
  private static final int MINI_DROID_MAXIMUM_DAMAGE = 100;
  private static final float MINI_DROID_PROBABILITY_OF_DOUBLE_SHOOT = 0.08f;

  private int miniDroid = 0;

  public Specialist() {
    super(HEALTH, MINIMUM_DAMAGE, MAXIMUM_DAMAGE, PROBABILITY_OF_DOUBLE_SHOOT,
        SPECIAL_ABILITY_CHARGE, COUNT_SHOOT_AND_MISS);
  }

  @Override
  public void shoot(Droid enemyDroid) {
    int damage = randomDamage();
    float randomFloat = (float) Math.random();

    if (performDoubleShoot()) {
      damage += damage;
      GamePanel.printMessages("Double shoot");
    }
    GamePanel.printMessages(getHeroName() + "'s damage = " + damage);
    enemyDroid.reduceHealthBy(damage);

    if (miniDroid != 0) {
      damage = ThreadLocalRandom.current()
          .nextInt(MINI_DROID_MINIMUM_DAMAGE, MINI_DROID_MAXIMUM_DAMAGE);
      if (MINI_DROID_PROBABILITY_OF_DOUBLE_SHOOT > randomFloat) {
        damage += damage;
        GamePanel.printMessages("Double shoot of mini droid");
      }
      GamePanel.printMessages("Damage of mini droid = " + damage);
      enemyDroid.reduceHealthBy(damage);
      miniDroid--;
    }
  }

  public void superAbility(Droid enemyDroid) {
    miniDroid = 3;
    GamePanel.printMessages("Mini droid will help " + getHeroName() + " in 3 move");
    resetSpecialAbilityCharge();
  }

  public int getMiniDroidMinimumDamage() {
    return MINI_DROID_MINIMUM_DAMAGE;
  }

  public boolean hasMiniDroid() {
    return miniDroid > 0;
  }
}
