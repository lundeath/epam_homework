package com.epam.droids;

import java.util.concurrent.ThreadLocalRandom;

public abstract class Droid {

  private String heroName;

  private int health;
  private int damageMin;
  private int damageMax;
  private float probabilityOfDoubleShoot;
  private final int specialAbilityChargedAmount;
  private int currentSpecialAbilityCharge;
  private int currentCountOfShootAndMiss;


  public Droid(int health, int damageMin, int damageMax, float probabilityOfDoubleShoot,
      int specialAbilityCharge, int currentCountOfShootAndMiss) {
    this.health = health;
    this.damageMin = damageMin;
    this.damageMax = damageMax;
    this.probabilityOfDoubleShoot = probabilityOfDoubleShoot;
    this.specialAbilityChargedAmount = specialAbilityCharge;
    currentSpecialAbilityCharge = specialAbilityChargedAmount;
    this.currentCountOfShootAndMiss = currentCountOfShootAndMiss;
  }

  public abstract void shoot(Droid enemyDroid);

  public abstract void superAbility(Droid droid);

  public void setHeroName(String heroName) {
    this.heroName = heroName;
  }

  public String getHeroName() {
    return heroName;
  }

  public void reduceHealthBy(int damage) {
    health -= damage;
  }

  public void restoreHealthBy(int amount) {
    health += amount;
  }

  public void chargeSpecialAbility() {
    if (!isSpecialAbilityReady()) {
      currentSpecialAbilityCharge++;
    }
  }

  public boolean isSpecialAbilityReady() {
    return currentSpecialAbilityCharge >= specialAbilityChargedAmount;
  }

  public boolean hasShootAndMiss() {
    return currentCountOfShootAndMiss != 0;
  }

  public void resetShootAndMissCount() {
    currentCountOfShootAndMiss--;
  }

  public void resetSpecialAbilityCharge() {
    currentSpecialAbilityCharge -= 3;
  }

  public int getCurrentSpecialAbilityCharge() {
    return currentSpecialAbilityCharge;
  }

  public int getCurrentCountOfShootAndMiss() {
    return currentCountOfShootAndMiss;
  }

  public int getHealth() {
    return health;
  }

  public int getDamageMin() {
    return damageMin;
  }

  protected int randomDamage() {
    return ThreadLocalRandom.current().nextInt(damageMin, damageMax);
  }

  protected boolean performDoubleShoot() {
    float randomFloat = (float) Math.random();
    return probabilityOfDoubleShoot > randomFloat;
  }

}
