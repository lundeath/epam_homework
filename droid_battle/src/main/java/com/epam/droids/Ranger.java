package com.epam.droids;

import com.epam.battle.GamePanel;

public class Ranger extends Droid {

  private static final int HEALTH = 1000;
  private static final int MINIMUM_DAMAGE = 60;
  private static final int MAXIMUM_DAMAGE = 120;
  private static final float PROBABILITY_OF_DOUBLE_SHOOT = 0.08f;
  private static final int SPECIAL_ABILITY_CHARGE = 3;
  private static final int COUNT_SHOOT_AND_MISS = 3;


  public Ranger() {
    super(HEALTH, MINIMUM_DAMAGE, MAXIMUM_DAMAGE, PROBABILITY_OF_DOUBLE_SHOOT,
        SPECIAL_ABILITY_CHARGE, COUNT_SHOOT_AND_MISS);
  }

  @Override
  public void superAbility(Droid enemyDroid) {
    int damage = randomDamage();
    damage = damage * 2;
    GamePanel
        .printMessages("" + getHeroName() + " use mega shoot",
            "" + getHeroName() + "'s damage = " + damage
        );
    enemyDroid.reduceHealthBy(damage);
    resetSpecialAbilityCharge();
  }

  @Override
  public void shoot(Droid enemyDroid) {
    int damage = randomDamage();
    int stolenHealth;
    if (performDoubleShoot()) {
      damage += damage;
      GamePanel.printMessages("Double shoot");
    }
    stolenHealth = damage / 10;
    restoreHealthBy(stolenHealth);
    GamePanel.printMessages("" + getHeroName() + "'s damage = " + damage,
        "Stolen " + stolenHealth + " HP from enemy"
    );
    enemyDroid.reduceHealthBy(damage);
  }
}
