package com.epam.droids;

import com.epam.battle.GamePanel;

public class Warrior extends Droid {

  private static final int HEALTH = 1250;
  private static final int MINIMUM_DAMAGE = 50;
  private static final int MAXIMUM_DAMAGE = 100;
  private static final float PROBABILITY_OF_DOUBLE_SHOOT = 0.09f;
  private static final int SPECIAL_ABILITY_CHARGE = 3;
  private static final int COUNT_SHOOT_AND_MISS = 3;


  public Warrior() {
    super(HEALTH, MINIMUM_DAMAGE, MAXIMUM_DAMAGE, PROBABILITY_OF_DOUBLE_SHOOT,
        SPECIAL_ABILITY_CHARGE, COUNT_SHOOT_AND_MISS);
  }

  @Override
  public void superAbility(Droid enemyDroid) {
    int damage = randomDamage();
    damage = damage * 2;
    GamePanel.printMessages(getHeroName() + " use crush shoot",
        getHeroName() + "'s damage = " + damage);
    enemyDroid.reduceHealthBy(damage);
    resetSpecialAbilityCharge();
  }

  @Override
  public void shoot(Droid enemyDroid) {
    int damage = randomDamage();
    if (damage > 90) {
      recoverHealth();
    }
    if (performDoubleShoot()) {
      damage += damage;
      GamePanel.printMessages("Double shoot");
      System.out.println("#  Double shoot");
    }
    GamePanel.printMessages(getHeroName() + "'s damage = " + damage);
    enemyDroid.reduceHealthBy(damage);
  }

  private void recoverHealth() {
    restoreHealthBy(20);
    GamePanel.printMessages("Health +20");
  }

}
