package com.epam.menu;

import com.epam.battle.Battle;
import com.epam.battle.GamePanel;
import com.epam.droids.Droid;
import com.epam.droids.Ranger;
import com.epam.droids.Specialist;
import com.epam.droids.Warrior;
import com.epam.hero.Hero;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Orest on 27.05.2018.
 */
public class Menu {

  public Menu() {
  }

  public void start() {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    String choice;
    while (true) {
      GamePanel.printMenuOptions();
      try {
        choice = reader.readLine();
        createHeroAndStartBattle(choice);
      } catch (IOException exception) {
        exception.printStackTrace();
      }
    }
  }

  private void createHeroAndStartBattle(String choice) {
    Droid myDroid = null;
    Hero enemyHero;
    switch (choice) {
      case "1":
        myDroid = new Ranger();
        break;
      case "2":
        myDroid = new Specialist();
        break;
      case "3":
        myDroid = new Warrior();
        break;
      case "4":
        System.exit(0);
        break;
      default:
        return;
    }
    myDroid.setHeroName("Player");
    enemyHero = new Hero();
    GamePanel.printMessages("You fight against "+enemyHero.getDroid().getClass().getSimpleName());
    new Battle(myDroid, enemyHero.getDroid());
  }

}
