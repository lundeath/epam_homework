package com.epam.battle;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class GamePanel {

  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_RED = "\u001B[31m";
  private static final String ANSI_YELLOW = "\u001B[33m";
  private static final String ANSI_VIOLET = "\u001B[35m";


  static {
    try {
      System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out), true, "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      throw new InternalError("VM does not support mandatory encoding UTF-8");
    }
  }

  public static void printDroidDetails(int botHealth, int myHealth, int botAbility,
      int myAbility, int botShootAndMiss, int myShootAndMiss) {

    drawTableHead(40);

    System.out
        .printf("\u2502  Your health: " + ANSI_RED + "%-24d" + ANSI_RESET + " \u2502\n", myHealth);
    System.out
        .printf("\u2502  Bot health: " + ANSI_RED + "%-25d" + ANSI_RESET + " \u2502\n", botHealth);

    drawTableMiddle(40);

    System.out.printf(
        "\u2502  Your current ability amount: " + ANSI_YELLOW + "%-8d" + ANSI_RESET + " \u2502\n",
        myAbility);
    System.out.printf(
        "\u2502  Bot current ability amount: " + ANSI_YELLOW + "%-9d" + ANSI_RESET + " \u2502\n",
        botAbility);

    drawTableMiddle(40);

    System.out.printf(
        "\u2502  Your count S&M: " + ANSI_VIOLET + "%-21d" + ANSI_RESET + " \u2502\n",
        myShootAndMiss);
    System.out.printf(
        "\u2502  Bot count S&M: " + ANSI_VIOLET + "%-22d" + ANSI_RESET + " \u2502\n",
        botShootAndMiss);

    drawTableBottom(40);

  }

  public static void printDroidOptions(ArrayList<String> options) {
    drawTableHead(20);
    System.out.println("\u2502       Options      \u2502");
    drawTableMiddle(20);
    for(String option:options){
      System.out.printf("\u2502 %-19s\u2502\n",option);
    }
//    System.out.println("\u2502  1) Shoot          \u2502");
//    if (isAbility) {
//      System.out.println("\u2502  2) Ability        \u2502");
//    }
//    if (isShootAndMiss) {
//      System.out.println("\u2502  3) Shoot&Miss     \u2502");
//    }
    drawTableBottom(20);
  }

  public static void printMenuOptions() {
    drawTableHead(20);
    System.out.println("\u2502  Choose your hero  \u2502");
    drawTableMiddle(20);
    System.out.println("\u2502 1.Ranger           \u2502");
    System.out.println("\u2502 2.Specialist       \u2502");
    System.out.println("\u2502 3.Warrior          \u2502");
    System.out.println("\u2502 4.Exit             \u2502");
    drawTableBottom(20);
  }

  public static void printMessages(String... messages) {
    drawTableDoubleHead(40);
    for (String message : messages) {
      System.out.printf("\u2551 %-38s \u2551\n", message);
    }
    drawTableDoubleBottom(40);
  }

  public static void printMessages(String message) {
    drawTableDoubleHead(40);
    System.out.printf("\u2551 %-38s \u2551\n", message);
    drawTableDoubleBottom(40);
  }

  private static void drawVerticalLine(int length) {
    for (int i = 0; i < length; i++) {
      System.out.print('\u2500');
    }
  }

  private static void drawDoubleVerticalLine(int length) {
    for (int i = 0; i < length; i++) {
      System.out.print('\u2550');
    }
  }

  private static void drawTableHead(int length) {
    System.out.print('\u250C');
    drawVerticalLine(length);
    System.out.println('\u2510');
  }

  private static void drawTableDoubleHead(int length) {
    System.out.print('\u2554');
    drawDoubleVerticalLine(length);
    System.out.println('\u2557');
  }

  private static void drawTableMiddle(int length) {
    System.out.print('\u251C');
    drawVerticalLine(length);
    System.out.println('\u2524');
  }

  private static void drawTableDoubleMiddle(int length) {
    System.out.print('\u2560');
    drawDoubleVerticalLine(length);
    System.out.println('\u2563');
  }

  private static void drawTableBottom(int lenght) {
    System.out.print('\u2514');
    drawVerticalLine(lenght);
    System.out.println('\u2518');
  }

  private static void drawTableDoubleBottom(int lenght) {
    System.out.print('\u255A');
    drawDoubleVerticalLine(lenght);
    System.out.println('\u255D');
  }
}
