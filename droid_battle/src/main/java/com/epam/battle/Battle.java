package com.epam.battle;

import com.epam.droids.Droid;
import com.epam.droids.Specialist;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Battle {

  static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

  public Battle(Droid myDroid, Droid enemyDroid) {
    battle(myDroid, enemyDroid);
  }

  private void myShoot(Droid myDroid, Droid enemyDroid) {
    boolean key = true;
    try {
      while (key) {
        ArrayList<String> options = new ArrayList<>();
        options.add("1. Shoot");
        if (myDroid.isSpecialAbilityReady()) {
          options.add("2. Special");
        }
        if (myDroid.hasShootAndMiss()) {
          options.add((options.size() + 1) + ". S&M");
        }
        GamePanel.printDroidOptions(options);
        String choice = reader.readLine();
        try {
          int intChoice = Integer.parseInt(choice);
          if (intChoice > options.size()) {
            continue;
          }
          Map<String, String> optionsMap = options.stream()
              .map(option -> option.split(". "))
              .collect(Collectors.toMap(arr -> arr[0], arr -> arr[1]));
          String action = optionsMap.get(choice);
          System.out.println(action);
          switch (action) {
            case "Shoot":
              myDroid.shoot(enemyDroid);
              myDroid.chargeSpecialAbility();
              key = false;
              break;
            case "Special":
              myDroid.superAbility(enemyDroid);
              key = false;
              break;
            case "S&M":
              shootAndMiss(myDroid,enemyDroid);
              break;
            default:
          }
        } catch (NumberFormatException e) {
          continue;
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void enemyShoot(Droid botDroid, Droid playerDroid) {
    if (playerDroid.getHealth() <= botDroid.getDamageMin()) {
      botDroid.shoot(playerDroid);
      return;
    }
    if (botDroid instanceof Specialist) {
      Specialist specialist = (Specialist) botDroid;
      if (specialist.hasMiniDroid()) {
        if (playerDroid.getHealth() <=
            specialist.getDamageMin() + specialist.getMiniDroidMinimumDamage()) {
          botDroid.shoot(playerDroid);
          return;
        }
      }
    }
    if (botDroid.isSpecialAbilityReady()) {
      botDroid.superAbility(playerDroid);
    } else if (botDroid.hasShootAndMiss()) {
      int choose = ThreadLocalRandom.current().nextInt(1, 2);
      if (choose == 1) {
        shootAndMiss(botDroid, playerDroid);
        enemyShoot(botDroid, playerDroid);
      } else {
        botDroid.shoot(playerDroid);
      }
    } else {
      botDroid.shoot(playerDroid);
    }
  }

  private void battle(Droid myDroid, Droid enemyDroid) {
    while ((myDroid.getHealth() >= 0) && (enemyDroid.getHealth() >= 0)) {
      GamePanel.printMessages("Your turn");
      GamePanel.printDroidDetails(enemyDroid.getHealth(), myDroid.getHealth(),
          enemyDroid.getCurrentSpecialAbilityCharge(), myDroid.getCurrentSpecialAbilityCharge(),
          enemyDroid.getCurrentCountOfShootAndMiss(), myDroid.getCurrentCountOfShootAndMiss());
      myShoot(myDroid, enemyDroid);
      if (enemyDroid.getHealth() < 0) {
        continue;
      }
      GamePanel.printMessages("Enemy turn");

//      GamePanel.printDroidDetails(enemyDroid.getHealth(), myDroid.getHealth(),
//          enemyDroid.getCurrentSpecialAbilityCharge(), myDroid.getCurrentSpecialAbilityCharge(),
//          enemyDroid.getCurrentCountOfShootAndMiss(), myDroid.getCurrentCountOfShootAndMiss());
      enemyShoot(enemyDroid, myDroid);
    }
    if (enemyDroid.getHealth() < 0) {
      GamePanel.printMessages("Your health"+myDroid.getHealth(),"and you win!");
    } else {
      GamePanel.printMessages("Enemy health"+enemyDroid.getHealth(),"and you lose!");

    }
  }


  public static void shootAndMiss(Droid droid1, Droid droid2) {
    GamePanel.printMessages(droid1.getHeroName() + " use shoot and miss");
    droid1.shoot(droid2);
    droid1.chargeSpecialAbility();
    droid2.chargeSpecialAbility();
    droid1.resetShootAndMissCount();
//    GamePanel.printDroidDetails(droid2.getHealth(), droid1.getHealth(),
//        droid2.getCurrentSpecialAbilityCharge(), droid1.getCurrentSpecialAbilityCharge(),
//        droid2.getCurrentCountOfShootAndMiss(), droid1.getCurrentCountOfShootAndMiss());

  }

//  public static void myShootAndMiss(Droid myDroid, Droid enemyDroid) {
//    GamePanel.printMessages(myDroid.getHeroName() + " use shoot and miss");
//    myDroid.shoot(enemyDroid);
//    myDroid.chargeSpecialAbility();
//    enemyDroid.chargeSpecialAbility();
//    myDroid.resetShootAndMissCount();
//
//  }
//
//  public static void enemyShootAndMiss(Droid myDroid, Droid enemyDroid) {
//    GamePanel.printMessages(myDroid.getHeroName() + " use shoot and miss");
//    myDroid.shoot(enemyDroid);
//    myDroid.chargeSpecialAbility();
//    enemyDroid.chargeSpecialAbility();
//    myDroid.resetShootAndMissCount();
//
//  }
}
